public class Main {
    public static void main (String[] args)
    {
        createUser("Hanna", "Omygod", 876543, 380976543218l, "hanna@mail.com", "qwertyui");
        createUser("henry@mail.com", "asdfghjk");
        UserChild firstChild = new UserChild("firstchild@gmail.ru", "yuhgfvb", 700,6);
        System.out.println(firstChild.salary);
        firstChild.increaseSalary();
        System.out.println(firstChild.salary);
    }
    public static boolean validation (String email, String password)
    {
        if (email.contains("@") && password.length()>=8 && password.length()<=16)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void createUser(String firstName, String lastName, int workPhone, long mobilePhone, String email, String password)
    {
        if (validation(email, password))
        {
            User user = new User(firstName, lastName, workPhone, mobilePhone, email, password);
            System.out.println(user.firstName);
        }
    }
    public static void createUser(String email, String password) {
        if (validation(email, password))
        {
            User user = new User(email, password);
            System.out.println(user.email);
        }
    }

}

