public class UserChild extends User
{
    int salary;
    int experienceWork;
    public UserChild(String firstName, String lastName, int workPhone, long mobilePhone, String email, String password, int salary, int experienceWork)
    {
        super(firstName, lastName, workPhone, mobilePhone, email, password);
        this.salary = salary;
        this.experienceWork = experienceWork;
    }
    public UserChild(String email, String password, int salary, int experienceWork)
    {
        super(email, password);
        this.salary = salary;
        this.experienceWork = experienceWork;

    }
    public void increaseSalary()
    {
        if (experienceWork<2)
        {
            salary += salary/100*5;
        }
        else if (experienceWork>=2 && experienceWork<=5)
        {
            salary += salary/100*10;
        }
        else
        {
            salary += salary/100*15;
        }
    }

}
