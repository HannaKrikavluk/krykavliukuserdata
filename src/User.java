public class User {
    public String firstName;
    public String lastName;
    public int workPhone;
    public long mobilePhone;
    public String email;
    public String password;

    public User(String firstName, String lastName, int workPhone, long mobilePhone, String email, String password)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.workPhone = workPhone;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.password = password;
    }

    public User(String email, String password)
    {
        this.email = email;
        this.password = password;
    }
}

